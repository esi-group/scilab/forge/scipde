// Copyright (C) 2012 - Michael Baudin
// Copyright (C) 2008 - Fr�d�ric Legrand
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

function scipdelib = scipdeload()

    TOOLBOX_NAME = "scipde"
    TOOLBOX_TITLE = "Scipde"

    mprintf("Start %s\n",TOOLBOX_TITLE);

    etc_tlbx  = get_absolute_file_path(TOOLBOX_NAME+".start");
    etc_tlbx  = getshortpathname(etc_tlbx);
    root_tlbx = strncpy( etc_tlbx, length(etc_tlbx)-length("\etc\") );

    // Load  functions library
    // =============================================================================

    mprintf("\tLoad macros\n");
    pathmacros = pathconvert( root_tlbx ) + "macros" + filesep();
    scipdelib  = lib(pathmacros);
    clear pathmacros;

    // Load and add help chapter
    // =============================================================================

    if ( %t ) then
        if or(getscilabmode() == ["NW";"STD"]) then
            mprintf("\tLoad help\n");
            path_addchapter = pathconvert(root_tlbx+"/jar");
            if ( isdir(path_addchapter) <> [] ) then
                add_help_chapter(TOOLBOX_TITLE, path_addchapter, %F);
            end
        end
    end

    // Add demos
    // =============================================================================

    if ( %f ) then
        if or(getscilabmode() == ["NW";"STD"]) then
            mprintf("\tLoad demos\n");
            demoscript = TOOLBOX_NAME + ".dem.gateway.sce"
            pathdemos = pathconvert(fullfile(root_tlbx,"demos",demoscript),%f,%t);
            add_demo(TOOLBOX_TITLE,pathdemos);
        end
    end

    // ====================================================================
    // A Welcome message.

    mprintf("\tType ""help scipde_overview"" for quick start.\n");
    mprintf("\tType ""demo_gui()"" and search for ""%s"" for Demonstrations.\n",TOOLBOX_TITLE);

endfunction 

if ( isdef("scipdelib") ) then
    warning("	Library is already loaded (""ulink(); clear scipdelib;"" to unload.)");
    return;
end

scipdelib = scipdeload ();
clear scipdeload;
