// Copyright (C) 2008-2009 - INRIA - Michael Baudin
//
// This file must be used under the terms of the GNU LGPL license.





//
// buildmacros.sce --
//   Builder for the Low Discrepancy Scilab Toolbox
//

tbx_build_macros("lowdiscrepancy", get_absolute_file_path('buildmacros.sce'));

clear tbx_build_macros;
