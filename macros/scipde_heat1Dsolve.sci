// Copyright (C) 2012 - Michael Baudin
// Copyright (C) 2008 - Frédéric Legrand
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

function [U,tf,niter,dx]=scipde_heat1Dsolve(N,BCtype,BCval,coef,S,U,dt,t)
    // Solve a 1D diffusion equation
    //
    // Calling Sequence
    // [U,t,niter,dx]=scipde_heat1Dsolve(N,BCtype,BCval,coef,S,U0,dt,t)
    //
    // Parameters
    // N : 1-by-1 matrix of doubles, integer value, number of space points
    // BCtype : 2-by-1 matrix of strings, type of the boundary condition at x=0 and x=1, "neumann" or "dirichlet"
    // BCval : 2-by-1 matrix of doubles, value of the boundary condition at x=0 and x=1
    // coef : a k-by-2 matrix of doubles, the diffusion coefficients with format [[x1,D1];[x2,D2],...[xk,Dk]], where xk=1.
    // S : N-by-1 matrix of doubles, the sources
    // U0 : N-by-1 matrix of doubles, the initial state
    // t : 2-by-1 matrix of doubles, the initial and final time. t(1) is the inital time, t(2) is the final time.
    // dt : 1-by-1 matrix of doubles, the time step
    // U : N-by-1 matrix of doubles, the solution
    // tf : 1-by-1 matrix of doubles, the achieved final time
    // niter : 1-by-1 matrix of doubles, integer value, the number of iterations
    // dx : 1-by-1 matrix of doubles, the space step
    //
    // Description
    // Solves the 1-dimensional equation 
    //
    // <latex>
    // \begin{eqnarray}
    // \left\{
    // \begin{array}{l}
    // \frac{\partial u}{\partial t} = D \frac{\partial^{2} u}{\partial x^{2}} + S(x,t), \quad x\in[0,1], t\geq t_0 \\
    // \\
    // u(0,t) = u_L \textrm{ (Dirichlet) or } u_x(0,t) = u_L \textrm{ (Neumann)}, \quad t\geq t_0\\
    // u(1,t) = u_R \textrm{ (Dirichlet) or } u_x(1,t) = u_R \textrm{ (Neumann)}, \quad t\geq t_0\\
    // \\
    // u(x,t_0)  = u_0(x), \quad x\in[0,1].
    // \end{array}
    // \right.
    // \end{eqnarray}
    //</latex>
    //
    // where u(x,t) is the unknown state. 
    // This is the heat equation. 
	// Notice that this function considers a domain with length L=1. 
	//
	// The couple of variables BCtype and BCval manage the boundary conditions. 
	// BCval(1) is the boundary condition value at x=0 while 
	// BCval(2) is the boundary condition value at x=1. 
	//
	// The <literal>coef</literal> values store the diffusion coefficients. 
	// The first column contains the x coordinates, in increasing order. 
	// The second column contains the associated diffusion coefficients. 
	// The first coefficient, D(1)=coef(1,2) is applied for x in the interval 
	// [0,coef(1,1)]. 
	// The diffusion coefficient D(i+1)=coef(i,2), is for x in the 
	// interval [coef(i-1,1),coef(i,1)], for i=2,3,...,k. 
	// This is why we expect that the last value in the first column, coef(k,1) 
	// is equal to 1, since it represents the maximum possible x coordinate. 
    //
	// Numerical Method
	//
    // To solve this equation, we use a implicit Crank-Nicholson 
    // method method, which is a finite difference 
    // method of order 2 in space and order 1 in time.
    // This requires to solve a sparse system of linear equations, 
    // with N unknowns.
	//
	// TODO : periodic conditions
	//
	// TODO : dynamic boundary conditions with a callback
	//
	// TODO : initial conditions with a callback in the style of Matlab's pdepe
    //
    // Examples
	// // Example A
    // // Consider a plate, with initial temperature U=0. 
    // // At x=0, impose U=1 (i.e. heat at the left side) and 
    // // at x=1, impose U=0 (i.e. cold at the right side).
    // // There is no source.
    // // See how the heat diffuses to the right.
    // N=100;   
    // X=linspace(0,1,N)';
    // U=zeros(N,1);
    // S=zeros(N,1);
    // coef=[[1,1]];
    // BCtype = ["dirichlet","dirichlet"]
    // BCval = [1,0]
    // h=scf();
    // t=0;
    // [U1,t]=scipde_heat1Dsolve(N,BCtype,BCval,coef,S,U,0.0001,[t,0.001]);
    // plot(X,U1,"b-")
    // [U2,t]=scipde_heat1Dsolve(N,BCtype,BCval,coef,S,U1,0.001,[t,0.01]);
    // plot(X,U2,"r-")
    // [U3,t]=scipde_heat1Dsolve(N,BCtype,BCval,coef,S,U2,0.01,[t,0.1]);
    // plot(X,U3,"g-")
    // [U4,t]=scipde_heat1Dsolve(N,BCtype,BCval,coef,S,U3,0.1,[t,1]);
    // plot(X,U4,"k-")
    // legend(["t=0.001","t=0.01","t=0.1","t=1"]);
	// xtitle("Diffusion equation","X","T");
	//
	// // See in the "Heat 1D Tutorial" for more details 
	// // on this function.
    //
    // Bibliography
    // "Informatique Appliquée aux Sciences Physiques", Frédéric Legrand, http://www.f-legrand.fr/scidoc/docopera/numerique/euler/euler/euler.xml
    // 
    // Authors
    // Copyright (C) 2012 - Michael Baudin
    // Copyright (C) 2008 - Frédéric Legrand

    fname="scipde_heat1Dsolve"

    [lhs, rhs] = argn()
    apifun_checkrhs (fname, rhs , 8 )
    apifun_checklhs (fname, lhs , 0 : 4 )
    //
    apifun_checktype (fname, N , "N" , 1 , "constant" )
    apifun_checktype (fname, BCtype , "BCtype" , 2 , "string" )
    apifun_checktype (fname, BCval , "BCval" , 3 , "constant" )
    apifun_checktype (fname, coef , "coef" , 4 , "constant" )
    apifun_checktype (fname, S , "S" , 5 , "constant" )
    apifun_checktype (fname, U , "U" , 6 , "constant" )
    apifun_checktype (fname, dt , "dt" , 7 , "constant" )
    apifun_checktype (fname, t , "t" , 8 , "constant" )
    //
    h=scipde_heat1Dinit(N,BCtype,BCval,coef,S,dt)
    A = h.A
    B = h.B
    C = h.C
    // Loop of the times
    tfinal=t(2)
    niter = 0
    tf=t(1)
    while (tf<tfinal)
        U = lusolve(A,B*U+C);
        tf = tf+dt
        niter = niter + 1
    end
endfunction
