// Copyright (C) 2012 - Michael Baudin
// Copyright (C) 2008 - Frédéric Legrand
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

function [US,niter]=scipde_heat1Dsteady(N,BCtype,BCval,coef,S,U0,dt)
    // Stationnary state of a 1D diffusion equation
    //
    // Calling Sequence
    // [US,niter]=scipde_heat1Dsteady(N,BCtype,BCval,coef,S,U0)
    //
    // Parameters
    // N : 1-by-1 matrix of doubles, integer value, number of space points
    // BCtype : 2-by-1 matrix of strings, type of the boundary condition at x=0 and x=1, "neumann" or "dirichlet"
    // BCval : 2-by-1 matrix of doubles, value of the boundary condition at x=0 and x=1
    // coef : a k-by-2 matrix of doubles, the diffusion coefficients with format [[x1,D1];[x2,D2],...[xk,Dk]], where xk=1.
    // S : N-by-1 matrix of doubles, the sources
    // U0 : N-by-1 matrix of doubles, the initial state
    // dt : 1-by-1 matrix of doubles, the time step
    // US : N-by-1 matrix of doubles, the solution
    // niter : 1-by-1 matrix of doubles, integer value, the number of iterations
    //
    // Description
    // Solves the stationnary state of the 1-dimensional heat equation 
    // where u(x,t) is the unknown state. 
    // In other words, solves:
    //
    // du/du = 0
    //
    // Numerical method
    // 
    // To solve the stationnary state equation, we use the 
    // same discretization method as in scipde_heat1Dsolve. 
    // We could iterate on the same numerical method, until 
    // a stationnary point is found, but this requires, in general, 
    // a large number of iterations. 
    // Instead, we use the <literal>fsolve</literal> function 
    // in order to find a fixed point of the numerical 
    // method: this is generally much faster.
    //
    // The computation of the gradient is based on the 
    // exact derivative of the function f for which we 
    // search a zero. 
    // The sparse linear system of equation is solved based 
    // on the UMFPACK functions umf_lufact and umf_solve.
    //
    // In general, niter is low, say niter = 2 or 3.
    // It may happen that fsolve has difficulties to converge. 
    // This can be seen from the value of niter which goes up to 
    // niter = 10, 20 or more. 
    // Let f be the function for which we search a zero US 
    // such that f(US)=0.
    // If the following condition is true:
    //
    // norm(f(U0))/norm(f(US))>1.e-8
    //
    // and if fsolve has produced an unexpected value of 
    // its status flag (i.e. if info is different from 1), 
    // then a warning is generated.
    //
    // Examples
    // N=20;
    // X=linspace(0,1,N)';
    // S=X;
    // coef=[[1,1.]];
    // BCtype = ["dirichlet","dirichlet"];
    // BCval = [0,0];
    // dt=.0001;
    // U=zeros(N,1);
    // // Exact solution
    // E=(X-X.^3)/6;
    // //
    // h=scf();
    // plot(X,E,"k-");
    // [US,niter]=scipde_heat1Dsteady(N,BCtype,BCval,coef,S,U,dt);
    // plot(X,US,"m*")
    // legend(["Exact","Steady"]);
    // xtitle("Diffusion equation - N=20","X","U");
    //
    // Bibliography
    // "Informatique Appliquée aux Sciences Physiques", Frédéric Legrand, http://www.f-legrand.fr/scidoc/docopera/numerique/euler/euler/euler.xml
    // 
    // Authors
    // Copyright (C) 2012 - Michael Baudin
    // Copyright (C) 2008 - Frédéric Legrand

    fname="scipde_heat1Dsteady"

    [lhs, rhs] = argn()
    apifun_checkrhs (fname, rhs , 7 )
    apifun_checklhs (fname, lhs , 0 : 2 )
    //
    apifun_checktype (fname, N , "N" , 1 , "constant" )
    apifun_checktype (fname, BCtype , "BCtype" , 2 , "string" )
    apifun_checktype (fname, BCval , "BCval" , 3 , "constant" )
    apifun_checktype (fname, coef , "coef" , 4 , "constant" )
    apifun_checktype (fname, S , "S" , 5 , "constant" )
    apifun_checktype (fname, U0 , "U0" , 6 , "constant" )
    apifun_checktype (fname, dt , "dt" , 7 , "constant" )
    //
    function y=scipde_f(U,h)
        // The function which solves f(U)=0
        global __diffusion_niter__
        A = h.A
        B = h.B
        C = h.C
        U1 = lusolve(A,B*U+C)
        y = U1-U
        __diffusion_niter__ = __diffusion_niter__ + 1
    endfunction
    function J=scipde_g(U,h)
        // The gradient of scipde_f
        A = h.A
        B = h.B
        lupointer = umf_lufact(A);
        V = umf_lusolve(lupointer,full(B))
        J = V-speye(A)
        J = full(J)
    endfunction
    //
    h=scipde_heat1Dinit(N,BCtype,BCval,coef,S,dt)
    //
    global __diffusion_niter__
    __diffusion_niter__=0
    [US,v,info]=fsolve(U0,list(scipde_f,h),list(scipde_g,h))
    if (info<>1) then
        rdelta = norm(scipde_f(US,h))/norm(scipde_f(U,h))
        if (rdelta>1.e-8) then
            warning(msprintf("%s: Unable to achieve steady state.",fname))
            warning(msprintf("%s: fsolve returned info=%d",fname,info))
            warning(msprintf("%s: See ""help fsolve"" for details.",fname))
        end
    end
    niter = __diffusion_niter__
endfunction
